//import 'react-router-dom'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/App.css';
import {TopNavbar} from "./components/TopNavbar";
import {Records} from "./components/Records";
import {NewRecord,ConfirmRecord} from "./components/NewRecord";
import {Toaster} from "react-hot-toast";

function Resources() {
    return <>r</>
}

function App() {
    return <BrowserRouter>
        <Switch>
            <Route path='/admin'>

                <TopNavbar active={'/'}/>
                <Switch>
                    <Route path="/admin/resources" exact component={Resources}/>
                    <Route path="/admin/new" exact component={NewRecord}/>
                    <Route path="/admin/" exact component={Records}/>
                </Switch>
            </Route>
            <Route path='/confirm/:id' component={ConfirmRecord}/>

            <Route path='/'>

            </Route>
        </Switch>
        <Toaster/>
    </BrowserRouter>
}

export default App;
