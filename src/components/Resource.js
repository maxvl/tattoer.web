import '../styles/components/Resource.sass'
export function Resource(props) {

    const {type, size, quantity, availability} = props.resource

    const color = {
        'missing': '#DC3545',
        'deficient':'#e2a802',
        'available':'#28A745'
    }[availability]

    return <div className={'resource'} style={{color:color,borderColor:color}}>
        {size} {type} x {quantity}
    </div>

}
