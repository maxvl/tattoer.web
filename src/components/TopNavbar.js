import {Nav} from "react-bootstrap";
import {Link} from "react-router-dom";
import '../styles/components/TopNavbar.sass'
export function TopNavbar(props) {
    return (<Nav className="justify-content-center" activeKey="/home">
        <Nav.Item>
            <Nav.Link active={props.active === '/'} as={Link} to="/admin/">Записи</Nav.Link>
        </Nav.Item>
        <Nav.Item>
            <Nav.Link active={props.active === '/'} as={Link} to={"/admin/resources"}>Расходники</Nav.Link>
        </Nav.Item>
    </Nav>)
}
