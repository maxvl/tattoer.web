import {Component} from "react";
import '../styles/components/Record.sass'
import {CalendarCheck, Instagram, Phone, FileEarmarkImage, Brush, Plus} from 'react-bootstrap-icons';
import {Resource} from "./Resource";
import {Link} from "react-router-dom";
import {toast} from 'react-hot-toast'
import api from '../api'


function Record(props) {

    const {client, price, resources, date, id} = props.record
    return <div className={'record'} onClick={() => {
        navigator.clipboard.writeText('kate.valit.dev/record/' + id)
        toast.success('Ссылка скопирована')
    }}>
        <div className={'content'}>
            <img src='https://placekitten.com/120/180'/>
            <div className={'details'}>
                <div className={'heading'}>
                    <h5>{client.name}</h5>
                    <h5>{(price.design + price.tattoo)} ₽</h5>
                </div>
                <li>
                    <ul><CalendarCheck/> {date}</ul>
                    <ul><Phone/> <a href={'tel:' + client.telnum}>{client.telnum}</a></ul>
                    <ul><Instagram/> <a href={'https://instagram.com/' + client.inst}>@{client.inst}</a></ul>
                    {(price.design === 0 ? <></> : <ul><Brush/> {price.design} ₽</ul>)}

                    <ul><FileEarmarkImage/> {price.tattoo} ₽</ul>

                </li>
            </div>

        </div>
        <div className={'resources'}>
            {resources.map((x, i) => <Resource key={i} resource={x}/>)}
        </div>

    </div>
}

function AddButton() {
    return <Link to={'/admin/new'} style={{
        position: 'fixed',
        bottom: 15,
        right: 25,
        borderRadius: '50%',
        backgroundColor: '#007bff',
        color: 'white',
        width: '50px',
        height: '50px',
        textAlign: 'center',
        fontWeight: '700',
        fontSize: 40,


    }}><Plus style={{marginTop: -16}}/></Link>
}

export class Records extends Component {
    state = {records: []}

    async componentDidMount() {
        const records = await api.getRecords()
        this.setState((state) => ({...state, records: records}))
    }

    render() {

        return <div className={'records'}>
            {this.state.records.map(x => <Record key={x.id} record={x}/>)}
            <AddButton/>
        </div>

    }
}
