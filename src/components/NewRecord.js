import {Component} from "react";
import '../styles/components/NewRecord.sass'
//import {CalendarCheck, Instagram, Phone, FileEarmarkImage,Brush,Plus} from 'react-bootstrap-icons';
import {Button, Col, Form, InputGroup} from "react-bootstrap";
import {Resource} from "./Resource";
import moment from 'moment'
import api from '../api'
import {toast} from "react-hot-toast";
import {useParams} from "react-router-dom";

export const NewRecord = props => <RecordForm new/>;

export const ConfirmRecord = props => {
    const params = useParams()
    return <RecordForm mode='confirm' id={params.id}/>
}

function convertToViewModel(model) {
    return {
        image: undefined,
        name: model.client.name,
        inst: model.client.inst,
        tel: model.client.telnum,
        time: moment(model.date, ('dd D MMM  HH:mm')).format('hh:mm'),
        date: moment(model.date, ('dd D MMM  HH:mm')).format('YYYY-MM-DD'),
        price: model.price,
        resources: model.resources,
        resourcesForm: {}
    }
}

export class RecordForm extends Component {
    _data = {
        image: undefined,
        name: '',
        inst: '',
        tel: '',
        time: '',
        date: '',
        price: {
            design: 0,
            tattoo: 0
        },
        resources: [],
        resourcesForm: {
            size: 1,
            type: 'RL',
            quantity: 1
        }
    }


    async componentDidMount() {
        if (this.props.id) {
            this.setState(convertToViewModel(await api.getRecord(this.props.id)))

        }

    }

    constructor(props, context) {
        super(props, context);
        this.state = {
            ...this._data
        }
        this.handleOnChange = this.handleOnChange.bind(this);
        this.handleAddResource = this.handleAddResource.bind(this);
        this.handleSaveClick = this.handleSaveClick.bind(this);
    }


    handleOnChange(evt) {
        const name = evt.target.name;
        if (name.includes('price')) {
            const [, p] = name.split('.')
            const v = evt.target.value ? Number(evt.target.value) : 0

            this.setState((state) => ({...state, price: {...state.price, [p]: v}}))
        } else
            this.setState((state) => ({...state, [name]: evt.target.value}))

    }


    render() {

        const totalPrice = this.state.price.tattoo + this.state.price.design
        return <Form id={'newRecordForm'}>
            <Form.Group>
                <Form.Label>Имя</Form.Label>
                <Form.Control name='name' type="text" placeholder="Вася" value={this.state.name}
                              onChange={this.handleOnChange}/>
            </Form.Group>
            <Form.Group>
                <Form.Row>
                    <Col>
                        <Form.Label>Дата</Form.Label>
                        <Form.Control name='date' type="date" value={this.state.date}
                                      readOnly={this.props.mode === 'confirm'}
                                      onChange={this.handleOnChange}/>
                    </Col>
                    <Col>
                        <Form.Label>Время</Form.Label>
                        <Form.Control name='time' type="time" value={this.state.time}
                                      readOnly={this.props.mode === 'confirm'}
                                      onChange={this.handleOnChange}/>
                    </Col>
                </Form.Row>
            </Form.Group>
            <Form.Group>
                <Form.Label>Телефон</Form.Label>
                <InputGroup className="mb-2">
                    <InputGroup.Prepend>
                        <InputGroup.Text>+</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control name='tel' type={'text'} value={this.state.tel} onChange={this.handleOnChange}/>
                </InputGroup>
            </Form.Group>
            <Form.Group>
                <Form.Label>Instagram</Form.Label>
                <InputGroup className="mb-2">
                    <InputGroup.Prepend>
                        <InputGroup.Text>@</InputGroup.Text>
                    </InputGroup.Prepend>
                    <Form.Control name='inst' type={'text'} value={this.state.inst} onChange={this.handleOnChange}/>
                </InputGroup>
            </Form.Group>

            <Form.Row>
                {
                    this.props.mode !== 'confirm' &&
                    <>
                        <Col>
                            <Form.Label>Эскиз</Form.Label>
                            <InputGroup className="mb-2">
                                <Form.Control name='price.design' type="number" value={this.state.price.design}
                                              onChange={this.handleOnChange}/>
                                <InputGroup.Append>
                                    <InputGroup.Text> ₽</InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </Col>
                        <Col>
                            <Form.Label>Нанесение</Form.Label>
                            <InputGroup className="mb-2">
                                <Form.Control name='price.tattoo' type="number" value={this.state.price.tattoo}
                                              onChange={this.handleOnChange}/>
                                <InputGroup.Append>
                                    <InputGroup.Text> ₽</InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </Col>
                    </>}
                <Col>
                    <Form.Label>{this.props.mode !== 'confirm' ? 'Всего' : 'Цена'}</Form.Label>
                    <InputGroup className="mb-2">
                        <Form.Control type="number" readOnly={true}
                                      value={totalPrice}/>
                        <InputGroup.Append>
                            <InputGroup.Text> ₽</InputGroup.Text>
                        </InputGroup.Append>
                    </InputGroup>
                </Col>

            </Form.Row>
            {this.props.mode !== 'confirm' &&
            <>
                <Form.Label>Расходники</Form.Label>
                <div id={'resources-row'}>
                    <InputGroup className="mb-2">
                        <Form.Control as="select" custom
                                      onChange={(evt) => this.handleAddResourceChange(evt, 'size')}>>
                            {[1, 3, 5, 7, 9, 11, 14].map((x, i) => <option key={i}>{x}</option>)}
                        </Form.Control>
                        <Form.Control as="select" custom
                                      onChange={(evt) => this.handleAddResourceChange(evt, 'type')}>
                            <option>RL</option>
                            <option>RS</option>
                            <option>MG</option>
                            <option>CM</option>
                        </Form.Control>
                        <Form.Control type="number"
                                      onChange={(evt) => this.handleAddResourceChange(evt, 'quantity')}
                                      value={this.state.resourcesForm.quantity}>

                        </Form.Control>
                        <InputGroup.Append>
                            <Button onClick={this.handleAddResource}>
                                <strong>+</strong>
                            </Button>
                        </InputGroup.Append>
                    </InputGroup>
                    {this.state.resources.map((x, i) => <Resource key={i} resource={x}/>)}

                </div>
            </>}
            <Button id={'save'}
                    onClick={this.handleSaveClick}>{this.props.mode === 'confirm' ? 'Подвердить' : 'Сохранить'}</Button>
        </Form>
    }

    handleAddResource() {
        this.setState((state) => ({
            ...state,
            resources: [...state.resources, {
                size: state.resourcesForm.size,
                type: state.resourcesForm.type,
                quantity: state.resourcesForm.quantity,
                availability: 'available' //TODO
            }]
        }))
    }

    convertToEntity() {
        const state = this.state

        return {
            id: null,
            client: {
                name: state.name,
                inst: 'm4k4s',
                telnum: '79991414672'
            },
            price: {
                design: state.price.design,
                tattoo: state.price.tattoo,
            },
            date: moment(`${state.date} ${state.time}`, 'YYYY-MM-DD HH:mm').format('dd D MMM  HH:mm'),
            resources: state.resources
        }
    }

    handleAddResourceChange(evt, member) {
        const val = evt.target.value
        this.setState((state) => ({...state, resourcesForm: {...state.resourcesForm, [member]: val}}))
    }

    async handleSaveClick(evt) {
        switch (this.props.mode) {
            case 'edit': {
                return
            }
            case 'new': {
                const {id} = await api.createRecord({...this.convertToEntity(), status: 'unconfirmed'})
                //navigator.clipboard.writeText('https://kate.valit.dev/confirm/' + id)
                navigator.clipboard.writeText('http://localhost:3000/confirm/' + id)
                toast.success("Ссылка на подтверждение скопирована")
                return
            }
            case 'confirm': {
                await api.confirmRecord({...this.convertToEntity(), status: 'unconfirmed',id:this.props.id})
                toast.success("Запись подтверждена")

            }

        }

    }
}
