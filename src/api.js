import axios from 'axios'
const api = 'http://localhost:8000'
const createRecord = async record => (await axios.post(`${api}/admin/records`, record)).data;
const getRecords = async () => (await axios.get(`${api}/admin/records`)).data;

const getRecord = async (id) =>{return (await axios.get(`${api}/admin/records/${id}`)).data};

const confirmRecord = async (entity)=>{
    return await axios.put(`${api}/records/${entity.id}/confirm`,entity)
}
export default {
    confirmRecord,
    createRecord,
    getRecord,
    getRecords,
}
